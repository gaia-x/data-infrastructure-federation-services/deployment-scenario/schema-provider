##########
# Builders
##########
FROM python:3-buster as python-builder
ENV WEBSITE_PREFIX=https://schemas.abc-federation.gaia-x.community/
COPY toolchain /toolchain
COPY single-point-of-truth /single-point-of-truth
RUN pip install -Ur toolchain/requirements.txt

FROM ruby:2.7 as ruby-java-builder
RUN rm -rf /var/lib/apt/lists/* && \
  apt-get update && \
  apt-cache gencaches && \
  apt-get install -y default-jdk zip unzip && \
  apt install -y graphviz && \
  wget https://github.com/sparna-git/shacl-play/releases/download/0.5/shacl-play-app-0.5-onejar.jar -O shacl-play.jar

##########
# Jobs
##########
# Create SHACL files inside /yaml2shacl
FROM python-builder as create-shacl
WORKDIR /toolchain
RUN for dir in $(find /single-point-of-truth/yaml/* -maxdepth 0 -type d); do \
  python3 yaml2shacl.py "${dir}"; \
  python3 constraintIndex.py "${dir}"; \
  done \
  && echo "SHACL files Ok"

# Create JSON LD schema inside /yaml2json
FROM create-shacl as add-json-schemas
WORKDIR /toolchain
RUN for dir in $(find /single-point-of-truth/yaml/* -maxdepth 0 -type d); do \
  python3 yaml2json.py "${dir}"; \
  python3 createJsonIndex.py "${dir}"; \
  done \
  && echo "JSON LD schemas Ok"

# Create Ontologies inside /yaml2ontology
FROM add-json-schemas as add-ontologies
WORKDIR /toolchain/ontology_generation
RUN for dir in $(find /single-point-of-truth/yaml/* -maxdepth 0 -type d); do \
  python3 yaml2ttl.py "${dir}"; \
  done \
  && echo "Ontologies Ok"

# Validate created files
FROM add-ontologies as check-rdf-shacl
WORKDIR /toolchain
COPY implementation /implementation
RUN for dir in $(find /implementation/* -maxdepth 0 -type d); do \
  python3 check_shacl.py "${dir}"; \
  done \
  && echo "RDF SHACL syntax OK"

# Enrich /yaml2shacl dir with UML files
FROM ruby-java-builder as add-UML
COPY --from=add-ontologies /toolchain /toolchain
COPY --from=create-shacl /yaml2shacl /yaml2shacl
RUN echo "Create UML Visualization" && for filename in $(find /yaml2shacl/  -type f -name "*.ttl"); do \
  java -jar shacl-play.jar draw -i "$filename" -o "yaml2shacl/$(basename $(dirname "$filename"))/uml/$(basename "$filename" .ttl).png"; \
  done \
  && echo "UML generation OK"

# Create /toolchain/visualization/output directory
FROM node:15-buster as visualize-sd-amples
COPY --from=add-UML /toolchain /toolchain
COPY implementation /implementation
WORKDIR /toolchain/visualization
RUN for dir in $(find ../../implementation/* -maxdepth 0 -type d); do \
  ./build.sh "${dir}"; \
  done

# Create /sd-attributes directory
FROM python-builder as gen-attribute-tables
COPY single-point-of-truth /single-point-of-truth
RUN mkdir -p /sd-attributes && for dir in $(find /single-point-of-truth/yaml/* -maxdepth 0 -type d); do \
  mkdir -p sd-attributes/$(basename "$dir"); \
  python /toolchain/docgen/cli.py sd-attributes --srcYaml ${dir} --srcConcept conceptual-model --fileName "/sd-attributes/$(basename "$dir")/sd-attributes.md"; \
  done \
  && echo "Gen attributes OK"

# Create /widoco directory
FROM ruby-java-builder as gen-ontology-doc
ENV DOCS_DIR=documentation
COPY single-point-of-truth /single-point-of-truth
COPY --from=visualize-sd-amples /toolchain /toolchain
COPY --from=check-rdf-shacl /yaml2shacl /yaml2shacl
COPY --from=check-rdf-shacl /yaml2ontology /yaml2ontology
COPY --from=check-rdf-shacl /yaml2json /yaml2json
RUN bash /toolchain/create_widoco_doc.sh

FROM python:3-bullseye as generate-html
ENV WEBSITE_PREFIX=https://schemas.abc-federation.gaia-x.community/
ENV DOCS_DIR=documentation
COPY --from=gen-ontology-doc /toolchain /code/toolchain
COPY --from=gen-ontology-doc /widoco /code/widoco
COPY --from=check-rdf-shacl /yaml2shacl /code/yaml2shacl
COPY --from=check-rdf-shacl /yaml2ontology /code/yaml2ontology
COPY --from=check-rdf-shacl /yaml2json /code/yaml2json
COPY --from=gen-attribute-tables /sd-attributes /code/sd-attributes
COPY . /code
WORKDIR /code/
RUN git config --global --add safe.directory /code
RUN git config --global --add safe.directory /code/gaia-x-document-template
RUN pip install jinja2-cli
RUN bash ./toolchain/generate_html.sh
RUN bash ./toolchain/create_pages.sh

FROM nginx:latest
WORKDIR /app
COPY --from=generate-html /code/public /usr/share/nginx/html