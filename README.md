# Gaia-X ontology

## GitLab Structure

Folders:

- `single-point-of-truth`: Single point of truth with respect to Self-Description Schema. Schema contains of a hirarchy of classes, called taxonomy and a set of attributes for each class. Attributes are encoded in YAML files.
- `documentation`: All documentation regarding SDs.
- `implementation`: All SD-related vocabularies, templates, examples, shapes/rules, and queries.
- `toolchain`: Toolchain facilitating creation, validation and visualization of Self-Descriptions as well as scripts of our CI/CD pipeline.

Each folder contains the different ontology versions of the Trust-Framework.

## GitLab Branches

There is one main branch on the repository:

- `develop`: stable branch

## How to contribute ?

- Open an issue
- Create a branch with your proposed changes
- Propose merge request on `develop`.

### Setup

To install the required tools, you can use [Nix](https://nixos.org/download.html).

Once installed:

- Install [Direnv](https://direnv.net/docs/installation.html) with Nix: `nix-env -iA nixpkgs.direnv`
- Copy `.envrc.example` to `.envrc`
- Allow Direnv to load you `.envrc` file: `direnv allow .`

## Output presentation

| Description                                                          |                                22.04 Link                                 |                                WIP Link                                 |
| -------------------------------------------------------------------- | :-----------------------------------------------------------------------: | :---------------------------------------------------------------------: |
| Ontology documentation with entities attributes of conceptual model. | https://schemas.abc-federation.gaia-x.community/22.04/sd-attributes.html  | https://schemas.abc-federation.gaia-x.community/wip/sd-attributes.html  |
| Visual representation of entities composing the ontology.            | https://schemas.abc-federation.gaia-x.community/22.04/ontology-index.html | https://schemas.abc-federation.gaia-x.community/wip/ontology-index.html |
| SHACL with Turtle syntax for self-description                        |  https://schemas.abc-federation.gaia-x.community/22.04/shacl-shapes.html  |  https://schemas.abc-federation.gaia-x.community/wip/shacl-shapes.html  |
| Json-ld schema for self-description                                  |    https://schemas.abc-federation.gaia-x.community/22.04/json-ld.html     |    https://schemas.abc-federation.gaia-x.community/wip/json-ld.html     |
| Compliance proposition                                               |                                                                           |   https://schemas.abc-federation.gaia-x.community/wip/compliance.html   |

## To understand concept

- https://www.w3.org/TR/shacl/
- https://www.w3.org/TR/turtle/

## Interesting repositories

### GX-FS Service characteristics WG

- [Git repository](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/tree/main/single-point-of-truth)
- [Website](https://gaia-x.gitlab.io/technical-committee/service-characteristics/v1.0/index.html)
  - [Downloads from Website](https://gaia-x.gitlab.io/technical-committee/service-characteristics/v1.0/downloads/)
  - [Participant Ontology](https://gaia-x.gitlab.io/technical-committee/service-characteristics/widoco/participant/participant.html)
  - [Service Ontology](https://gaia-x.gitlab.io/technical-committee/service-characteristics/widoco/service/service.html)

### GX SD-Model

[Git repository](https://gitlab.com/gaia-x/technical-committee/federation-services/self-description-model)

## Tools

- [SD Generator from SHACL](https://gaia-x.fit.fraunhofer.de/)
- [JSON playground](https://json-ld.org/playground/)
- [SHACL playground](https://shacl.org/playground/)


