.PHONY: all tests check build validate doc page

current_dir = $(shell pwd)

#all: tests check build validate doc page
all: tests check build validate doc page

build: create-shacl create-json create-ontology

validate: check-rdf-shacl

doc: gen-UML visualize-sd-amples gen-attribute-tables gen-ontology-doc

page: generate-html

tests:
	gitlab-runner exec docker toolchain-unittests

check:
	gitlab-runner exec docker check-yaml-syntax
	gitlab-runner exec docker check-yaml-semantic

create-shacl:
	docker volume rm -f yaml2shacl
	gitlab-runner exec docker create-shacl --docker-volumes 'yaml2shacl:/yaml2shacl' --post-build-script "mv ../yaml2shacl/* /yaml2shacl"
	rm -rf yaml2shacl
	mkdir -p yaml2shacl
	docker run --rm -v yaml2shacl:/yaml2shacl -v ${current_dir}/yaml2shacl:/out ubuntu bash -c "cp -r /yaml2shacl/* /out/"

create-json:
	docker volume rm -f yaml2json
	gitlab-runner exec docker create-json --docker-volumes 'yaml2json:/yaml2json' --post-build-script "mv ../yaml2json/* /yaml2json"
	rm -rf yaml2json
	mkdir -p yaml2json
	docker run --rm -v yaml2json:/yaml2json -v ${current_dir}/yaml2json:/out ubuntu bash -c "cp -r /yaml2json/* /out/"

create-ontology:
	docker volume rm -f yaml2ontology
	gitlab-runner exec docker create-ontology --docker-volumes 'yaml2ontology:/yaml2ontology' --post-build-script "mv ../../yaml2ontology/* /yaml2ontology"
	rm -rf yaml2ontology
	mkdir -p yaml2ontology
	docker run --rm -v yaml2ontology:/yaml2ontology -v $(current_dir)/yaml2ontology:/out ubuntu bash -c "cp -r /yaml2ontology/* /out/"

check-rdf-shacl:
	gitlab-runner exec docker check-rdf-shacl --docker-volumes 'yaml2shacl:/yaml2shacl' --pre-build-script "cp -r /yaml2shacl ./"

gen-UML: create-shacl
	gitlab-runner exec docker gen-UML --docker-volumes 'yaml2shacl:/yaml2shacl' --pre-build-script "cp -r /yaml2shacl ./" --post-build-script "cp -r ./yaml2shacl/* /yaml2shacl"
	docker run --rm -v yaml2shacl:/yaml2shacl -v $(current_dir)/yaml2shacl:/out ubuntu bash -c "cp -r /yaml2shacl/* /out/"
	
visualize-sd-amples: 	
	docker volume rm -f visu-sd
	gitlab-runner exec docker visualize-sd-amples --docker-volumes 'visu-sd:/visu-sd' --post-build-script "mv output/* /visu-sd" 
	rm -rf toolchain/visualization/output
	mkdir -p toolchain/visualization/output
	docker run --rm -v visu-sd:/visu-sd -v $(current_dir)/toolchain/visualization/output:/out ubuntu bash -c "cp -r /visu-sd/* /out/"

gen-attribute-tables:
	docker volume rm -f sd-attributes
	gitlab-runner exec docker gen-attribute-tables --docker-volumes 'sd-attributes:/sd-attributes' --post-build-script "pwd && ls -ltrah && cp -r ./sd-attributes/* /sd-attributes" 
	rm -rf sd-attributes
	mkdir -p sd-attributes
	docker run --rm -v sd-attributes:/sd-attributes -v $(current_dir)/sd-attributes:/out ubuntu bash -c "cp -r /sd-attributes/* /out/"

gen-ontology-doc: #build visualize-sd-amples
	docker volume rm -f widoco
	gitlab-runner exec docker gen-ontology-doc --docker-volumes 'widoco:/widoco' --docker-volumes 'yaml2shacl:/yaml2shacl' --docker-volumes 'yaml2json:/yaml2json' --docker-volumes 'yaml2ontology:/yaml2ontology' --docker-volumes 'visu-sd:/visu-sd' --pre-build-script "cp -r /yaml2shacl ./ && cp -r /yaml2json ./ && cp -r /yaml2ontology ./ && cp -r /visu-sd/ ./toolchain/visualization/output/" --post-build-script "cp -r ./widoco/* /widoco"
	rm -rf widoco
	mkdir -p widoco
	docker run --rm -v widoco:/widoco -v $(current_dir)/widoco:/out ubuntu bash -c "cp -r /widoco/* /out/"

generate-html: #doc
	docker volume rm -f landing-page
	gitlab-runner exec docker generate-html --docker-volumes 'widoco:/widoco' --docker-volumes 'landing-page:/landing-page' --docker-volumes 'sd-attributes:/sd-attributes' --docker-volumes 'yaml2shacl:/yaml2shacl' --docker-volumes 'yaml2json:/yaml2json' --docker-volumes 'yaml2ontology:/yaml2ontology' --docker-volumes 'visu-sd:/visu-sd' --pre-build-script "cp -r /widoco ./ && cp -r /yaml2shacl ./ && cp -r /yaml2json ./ && cp -r /sd-attributes ./ && cp -r /yaml2ontology ./ && cp -r /visu-sd/ ./toolchain/visualization/output/" --post-build-script "cp -r ./landing-page/* /landing-page"
	rm -rf landing-page
	mkdir -p landing-page
	docker run --rm -v landing-page:/landing-page -v $(current_dir)/landing-page:/out ubuntu bash -c "cp -r /landing-page/* /out/"

clean: #drop content created by ci chain
	rm -f toolchain/mergedDataGraph.ttl
	rm -rf yaml2json
	rm -rf yaml2shacl
	rm -rf yaml2ontology
	rm -rf widoco
	rm -rf sd-attributes
	rm -f shacl-play.jar
