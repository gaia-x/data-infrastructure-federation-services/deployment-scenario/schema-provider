#! bash

set -e

echo "Creating documentation..."
test -f widoco.jar || wget https://github.com/dgarijo/Widoco/releases/download/v1.4.15_1/widoco-1.4.15-jar-with-dependencies.jar -O widoco.jar
for dir in $(find ./single-point-of-truth/yaml/* -maxdepth 0 -type d); do
  mkdir -p widoco/$(basename "$dir")
  #mkdir -p widoco/$(basename "$dir")/provider/
  #cp implementation/$(basename "$dir")/instances/provider/*.ttl widoco/$(basename "$dir")/provider/ 2>/dev/null || true
  #cp implementation/$(basename "$dir")/instances/provider/*.jsonld widoco/$(basename "$dir")/provider/ 2>/dev/null || true

  cd widoco/$(basename "$dir") && find . -name \*.ttl -o -name \*.jsonld > files.txt
  cd -

  touch widoco/$(basename "$dir")/index.html
  for filename in yaml2ontology/$(basename "$dir")/*.ttl; do
    name_generated=$(basename "$filename" .ttl)
    name=${name_generated%_*}
    out_dir=widoco/$(basename "$dir")/$name/
    mkdir -p ${out_dir}
    echo "Creating $name documentation..."
    java -jar widoco.jar -ontFile $filename -outFolder ${out_dir} -webVowl -uniteSections -rewriteAll
    sed -i '/<tr><td><b>gax-$name<\/b><\/td><td>&lt;http:\/\/w3id.org\/gaia-x\/$name#&gt;<\/td><\/tr>/d' ${out_dir}/index-en.html
    mv ${out_dir}/index-en.html ${out_dir}/index.html
    #rm ${out_dir}/webvowl/data/ontology.json
    java -jar ./toolchain/owl2vowl.jar -file ${out_dir}/ontology.xml
    #mv ${out_dir}/ontology.json ${out_dir}/webvowl/data/
  done

  echo "Create JSONLD Visualization"
  mkdir -p widoco/$(basename "$dir")/visualization/
  cp -r toolchain/visualization/output/$(basename "$dir")/* widoco/$(basename "$dir")/visualization/
  cp toolchain/constraints.html widoco/$(basename "$dir")/

  echo "# Ontology - $(basename ${dir})" > widoco/$(basename "$dir")/ontology-index.md
  echo '## OWL documentation (generated with Widoco)' >> widoco/$(basename "$dir")/ontology-index.md
  echo "Please find below the links to namespaced OWL documentation" >> widoco/$(basename "$dir")/ontology-index.md
  echo " " >> widoco/$(basename "$dir")/ontology-index.md
  for filename in yaml2ontology/$(basename "$dir")/*.ttl; do
    name_generated=$(basename "$filename" .ttl)
    name=${name_generated%_*}
    echo "* [${name} ontology](${WEBSITE_PREFIX}/$(basename ${dir})/vocab/${name}/index.html)" >> widoco/$(basename "$dir")/ontology-index.md
  done
  
  echo '## Turtle ontologies' >> widoco/$(basename "$dir")/ontology-index.md
  for filename in yaml2ontology/$(basename "$dir")/*.ttl; do
    name_generated=$(basename "$filename")
    echo "* [${name_generated}](${WEBSITE_PREFIX}/$(basename ${dir})/ontology-autogen/${name_generated})" >> widoco/$(basename "$dir")/ontology-index.md
  done
done