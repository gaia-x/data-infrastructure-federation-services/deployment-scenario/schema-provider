import os
import sys
import glob
import re

def createJsonIndex(directory:str):
    index = open("../yaml2json/%s/index.html" % (directory), 'w')
    index.write('<!DOCTYPE html>\n<html>\n<head>\n\t<title>Json-ld schema</title>\n</head>\n<body>\n\n<table border="2" cellspacing="0" cellpadding="10">\n')

    index.write('\t<tr>\n')
    index.write('\t\t<th>Json-ld schema</th>\n')
    index.write('\t</tr>\n')

    for filename in glob.glob('../yaml2json/%s/*.json' % (directory)):
        filename = filename.split('/')[-1].split('.')[0]
        index.write('\t<tr>\n')
        index.write('\t\t<td><a href="%s/%s/json-validation/%s.json">%s.json</a></td>\n' % (URL_PREFIX, directory, filename, filename))
        index.write('\t</tr>\n')

    index.write('\n</table>\n\n</body>\n</html>')

def createMarkdownJsonIndex(directory:str):
    index = open("../yaml2json/%s/json-ld.md" % (directory), 'w')
    index.write('# JSON schemas - {0}\n'.format(directory))
    
    for filename in glob.glob('../yaml2json/%s/*.json' % (directory)):
        filename = filename.split('/')[-1].split('.')[0]
        index.write('- [{0}.json]({1}/{2}/json-validation/{0}.json)\n'.format(filename, URL_PREFIX, directory))

if __name__ == "__main__":
    URL_PREFIX = os.environ.get("WEBSITE_PREFIX")
    URL_PREFIX = re.sub('/$', '', URL_PREFIX)
    createJsonIndex(os.path.basename(sys.argv[1]))
    createMarkdownJsonIndex(os.path.basename(sys.argv[1]))
    