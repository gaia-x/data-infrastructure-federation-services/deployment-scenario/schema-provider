import os
import sys
import glob
import re

def createIndex(directory:str):
    index = open("../yaml2shacl/%s/index.html" % (directory), 'w')
    index.write('<!DOCTYPE html>\n<html>\n<head>\n\t<title>SHACL Shapes</title>\n</head>\n<body>\n\n<table border="2" cellspacing="0" cellpadding="10">\n')

    index.write('\t<tr>\n')
    index.write('\t\t<th>SHACL Shape</th>\n')
    index.write('\t\t<th>UML Visualization</th>\n')
    index.write('\t</tr>\n')

    for filename in glob.glob('../yaml2shacl/%s/*.ttl' % (directory)):
        filename = filename.split('/')[-1].split('.')[0]
        
        index.write('\t<tr>\n')
        index.write('\t\t<td><a href="%s/%s/validation/%s.ttl">%s.ttl</a></td>\n' % (URL_PREFIX, directory, filename,filename))
        index.write('\t\t<td><img src="%s/%s/validation/uml/%s.png" alt="%s.png"></td>\n' % (URL_PREFIX, directory, filename,filename))
        index.write('\t</tr>\n')

    index.write('\n</table>\n\n</body>\n</html>')

def createMarkdownIndex(directory:str):
    index = open("../yaml2shacl/%s/shacl-shapes.md" % (directory), 'w')
    index.write('# SHACL Shapes - {0}\n'.format(directory))
    index.write('|SHACL Shapes|UML Visualization|\n')
    index.write('|-----|-----|\n')

    for filename in glob.glob('../yaml2shacl/%s/*.ttl' % (directory)):
        filename = filename.split('/')[-1].split('.')[0]
        index.write('|[{0}.ttl]({1}/{2}/validation/{0}.ttl)|![{0}.png]({1}/{2}/validation/uml/{0}.png)|\n'.format(filename, URL_PREFIX, directory))
    
if __name__ == "__main__":
    URL_PREFIX = os.environ.get("WEBSITE_PREFIX")
    URL_PREFIX = re.sub('/$', '', URL_PREFIX)
    createIndex(os.path.basename(sys.argv[1]))
    createMarkdownIndex(os.path.basename(sys.argv[1]))
    