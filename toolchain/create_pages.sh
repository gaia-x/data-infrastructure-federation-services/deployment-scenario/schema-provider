#! bash

set -e 

mkdir -p public/    
cp -r landing-page/* public/

# Copy individual SHACL TTL files
for dir in $(find ./yaml2shacl/* -maxdepth 0 -type d); do
  mkdir -p public/$(basename ${dir})/validation
  cp -r ${dir}/* public/$(basename ${dir})/validation
done

# Copy individual JSON-LD schema files
for dir in $(find ./yaml2json/* -maxdepth 0 -type d); do
  mkdir -p public/$(basename ${dir})/json-validation
  cp -r ${dir}/* public/$(basename ${dir})/json-validation
done

# Copy ontologies
for dir in $(find ./yaml2ontology/* -maxdepth 0 -type d); do
  mkdir -p public/$(basename ${dir})/ontology-autogen
  cp -r ${dir}/* public/$(basename ${dir})/ontology-autogen
done

# Copy widoco files
for dir in $(find ./widoco/* -maxdepth 0 -type d); do
  mkdir -p public/$(basename ${dir})/vocab
  cp -r ${dir}/* public/$(basename ${dir})/vocab
done
