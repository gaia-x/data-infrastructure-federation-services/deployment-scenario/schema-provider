#! bash

set -e

test -f requirements.txt && pip install -Ur requirements.txt
test -f gaia-x-document-template/requirements.txt || git submodule init; git submodule update --recursive
test -f gaia-x-document-template/requirements.txt && pip install -Ur gaia-x-document-template/requirements.txt
cp -r gaia-x-document-template/template_html/extra.css $DOCS_DIR/
cp -r gaia-x-document-template/template_html/extra.js $DOCS_DIR/
echo "CI_PROJECT_URL: '${CI_PROJECT_URL}'" > config.yaml
echo "DOCS_DIR: '${DOCS_DIR}'" >> config.yaml
echo "versions:" >> config.yaml
for dir in $(find ./single-point-of-truth/yaml/* -maxdepth 0 -type d); do
  echo "  '$(basename "$dir")':" >> config.yaml
  mkdir -p $DOCS_DIR/$(basename "$dir")
  cp -r gaia-x-document-template/template_html/extra.css $DOCS_DIR/$(basename "$dir")/
  cp -r gaia-x-document-template/template_html/extra.js $DOCS_DIR/$(basename "$dir")/
  cp sd-attributes/$(basename "$dir")/sd-attributes.md $DOCS_DIR/$(basename "$dir")/
  cp yaml2shacl/$(basename "$dir")/shacl-shapes.md $DOCS_DIR/$(basename "$dir")/
  cp yaml2json/$(basename "$dir")/json-ld.md $DOCS_DIR/$(basename "$dir")/
  cp widoco/$(basename "$dir")/ontology-index.md $DOCS_DIR/$(basename "$dir")/
  test -d contexts/$(basename "$dir") && cp -r contexts/$(basename "$dir") $DOCS_DIR/$(basename "$dir")/contexts
  # Add files prefixed inside in docs/$(basename "$dir")  in documentation_items
  
  if test -d ./docs/$(basename "$dir"); then
    echo "    documentation_items:" >> config.yaml
    for f in $(find ./docs/$(basename "$dir")/* -maxdepth 0 -type f); do
      cp ${f} $DOCS_DIR/$(basename "$dir")/
      echo "    - '$(basename "$dir")/$(basename "$f")'" >> config.yaml
    done
  fi
done
test -f generate_html_custom.sh && bash ./generate_html_custom.sh 
jinja2 mkdocs.yml.j2 config.yaml --format yaml > mkdocs.yml
cat mkdocs.yml
mkdocs build --no-directory-urls
test -f pre_publish_html.sh && bash pre_publish_html.sh
mkdir -p landing-page
cp -r public/* landing-page
echo "done" # If last command in script is 'test -f somefile', job fails