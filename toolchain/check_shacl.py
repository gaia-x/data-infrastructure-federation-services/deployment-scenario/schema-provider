#!/usr/bin/env python3
import os
import sys
import pyshacl
import rdflib
from colorama import Fore, Style
import glob

def mergeDataGraphs():
    graph = rdflib.Graph()
    for filename in glob.glob(os.path.join(IMPLEMENTATION_DIR,'instances/*/*.jsonld')):
        print(Style.BRIGHT + Fore.GREEN + "Parse file {}".format(filename) + Style.RESET_ALL)
        graph.parse(filename, format='json-ld')
    graph.serialize('mergedDataGraph.ttl', format='ttl')

def validation():

    err_count = 0
    for filename in glob.glob(os.path.join(SHACL_DIR, '*.ttl')):
        print(Style.BRIGHT + Fore.BLUE + "Validate SHACL file {}".format(filename) + Style.RESET_ALL)
        r = pyshacl.validate('mergedDataGraph.ttl', shacl_graph=filename)
        conforms, results_graph, results_text = r
        if not conforms:
            print(results_text)
            err_count += 1
    return err_count


if __name__ == '__main__':

    IMPLEMENTATION_DIR = os.path.abspath(sys.argv[1])
    SHACL_DIR = os.path.abspath("../yaml2shacl/"+ os.path.basename(sys.argv[1])+"/")

    print("Starting SHACL validation for " + SHACL_DIR + " folder, using implementation: " + IMPLEMENTATION_DIR)

    mergeDataGraphs()
    err_count = validation()

    if err_count > 0:
        print(Style.BRIGHT + Fore.RED + 'found {} error(s)'.format(err_count) + Style.RESET_ALL)
    os.sys.exit(err_count)

