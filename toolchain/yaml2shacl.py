import sys
import yaml
import re
import os
import fnmatch
from jinja2 import Environment, FileSystemLoader

def parseYAML(input):
    with open(input, "r") as stream:
        try:
            data = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
    return data


def getName(title):
    words = re.split('(?=[A-Z])', title)
    name = ''
    for word in words:
        name += word + ' '
    name = name[:-1]
    return name.lower()


def addSuperClasses(classNames, ignoreClasses=None):
    """
    For a given list of classes, recursively find and return their yaml sources.
    """
    datas = []
    if ignoreClasses is None:
        ignoreClasses = set()
    superClasses = set(classNames) - set(ignoreClasses)
    files = getYamlFilesFromFolder(YML_DIR_PATH, True)
    for className in superClasses:
        for filename in files:
            if className.lower() in filename.lower().replace('-', ''):
                data = parseYAML(filename)
                if isCorrectYamlForClass(data, className):
                    ignoreClasses.add(className)
                    datas.append(data)
                    newSuperClassNames = getSuperClassesFromYaml(data)
                    datas += (addSuperClasses(newSuperClassNames, ignoreClasses.union(set(classNames))))
    return datas


def appendObjectShapes(objectReferences, file, ignoreObjects=None):
    """
    Recursively append shapes for referenced objects to the output file
    """
    if ignoreObjects is None:
        ignoreObjects = []
    objectReferences = set(objectReferences) - set(ignoreObjects)
    files = getYamlFilesFromFolder(getValidationYamlLocation(), False)
    for objectReference in objectReferences:
        prefix, suffix = objectReference.split(":")
        for filename in files:
            if suffix.lower() in filename.lower().replace('-', ''):
                data = parseYAML(filename)
                if isCorrectYamlForObject(data, objectReference):
                    names, newReferences = writeShapeToFile(data, file)
                    uniqueObjects = set(newReferences) - objectReferences
                    appendObjectShapes(uniqueObjects, file, set.union(objectReferences, ignoreObjects))


def getSuperClassesFromYaml(data):
    """
    For a given object from a YAML source, extract and return the referenced super classes.
    """
    superClassNames = []
    for key in [*data.keys()]:
        superClassNames += data[key]["subClassOf"]
    return superClassNames


def isCorrectYamlForObject(yamlData, objectReference):
    """
    Check for a given yaml definition and an object reference, if the suitable shape for the reference can be generated
    from the yaml
    """
    prefix, suffix = objectReference.split(":")
    yamlName = [*yamlData.keys()][0]
    yamlPrefix = yamlData[yamlName]["prefix"]
    if yamlName == suffix and yamlPrefix == prefix:
        return True
    return False


def isCorrectYamlForClass(yamlData, className):
    """
    For a given yaml definition and a class name, check if the yaml contains the data for the given class.
    """
    yamlName = [*yamlData.keys()][0]
    if yamlName == className:
        return True
    return False


def getShapeNameForObject(name):
    """
    For a given object reference name, find the suitable yaml definition and construct the name of the matching shape.
    """
    yamlLocation = getValidationYamlLocation()
    files = getYamlFilesFromFolder(yamlLocation, True)
    for file in files:
        data = parseYAML(file)
        if isCorrectYamlForObject(data, name):
            return "gax-validation:%sShape" % ([*data.keys()][0])
    return None


def writeShapeToFile(srcData, shaclFile):
    objectReferences = []
    names = []
    # Create shapes for each class defined in the YAML file
    for key in [*srcData.keys()]:
        prefix = srcData[key]["prefix"]
        superClassNames = srcData[key]["subClassOf"]
        superClassesDataList = addSuperClasses(superClassNames)
        dataList = [srcData] + superClassesDataList
        # Add sh:targetClass for the shape
        line = "\ngax-validation:%sShape\n\ta sh:NodeShape ;\n\tsh:targetClass %s:%s ;\n" % (
        key, prefix, key)
        names.append("%s:%s" % (prefix, key))
        shaclFile.write(line)
        for dataSet in dataList:
            key = [*dataSet.keys()][0]
            # Add constraints depending on the attributes defined in the YAML file
            order = 1
            for attribute in dataSet[key]["attributes"]:

                # Add sh:path constraint
                line = "\tsh:property [ sh:path %s:%s ;\n" % (attribute["prefix"], attribute["title"])
                shaclFile.write(line)

                # Add sh:name
                line = "\t\t\t\t  sh:name \"%s\" ;\n" % getName(attribute["title"])
                shaclFile.write(line)

                # Add sh:order
                line = "\t\t\t\t  sh:order %s ;\n" % order
                shaclFile.write(line)
                order += 1

                # Add sh:minCount / sh:maxCount constraints if defined in the YAML file
                if [*attribute.keys()].__contains__("cardinality"):
                    for cardinalityValue in [*attribute["cardinality"].keys()]:
                        line = "\t\t\t\t  sh:%s %s ;\n" % (cardinalityValue, attribute["cardinality"][cardinalityValue])
                        shaclFile.write(line)

                # Add either sh:datatype or sh:class constraint depending on the defined range of the property
                if attribute["dataType"].__contains__("xsd"):
                    line = "\t\t\t\t  sh:datatype %s ] ;\n" % attribute["dataType"]
                    shaclFile.write(line)
                elif [*attribute.keys()].__contains__("dataType"):
                    shapeName = getShapeNameForObject(attribute["dataType"])
                    if shapeName:
                        objectReferences.append(attribute["dataType"])
                        line = "\t\t\t\t  sh:node %s ] ;\n" % shapeName
                    elif attribute["dataType"] in simple_data_types:
                        line = "\t\t\t\t  sh:class %s ] ;\n" % attribute["dataType"]
                    else:
                        line = "\t\t\t\t  sh:nodeKind sh:IRI ] ;\n"
                    shaclFile.write(line)
        # Add dot to close the shape
        line = ".\n"
        shaclFile.write(line)
        return (names, objectReferences)


def writeShaclFile(prefixes, data, output):
    # Create empty SHACL file
    shaclFile = open(output, "w")

    if prefixes:
        # Add SHACL and gax-validation prefix
        line = "@prefix sh: <http://www.w3.org/ns/shacl#> .\n"
        shaclFile.write(line)
        line = "@prefix gax-validation:  <%s/validation#> .\n\n" % (URL_PREFIX)
        shaclFile.write(line)

        # Add all prefixes defined in the YAML file
        for prefix in prefixes["Prefixes"]:
            line = "@prefix %s: <%s> .\n" % (prefix["name"], prefix["value"])
            shaclFile.write(line)

    names, objectReferences = writeShapeToFile(data, shaclFile)
    appendObjectShapes(objectReferences, shaclFile, names)
    shaclFile.close()


def getYamlFilesFromFolder(path, includeSubdirectories=False):            
    files = []
    with os.scandir(os.path.abspath(path)) as it:
        for entry in it:
            if entry.is_file():
                name, file_extension = os.path.splitext(entry.name)
                if entry.name.endswith('.yaml') and entry.name != "dataTypeAbbreviation.yaml" and entry.name != "prefixes.yaml":
                    files.append(entry.path)
            if entry.is_dir() and entry.name.startswith('gax-') and includeSubdirectories:
                files.extend(getYamlFilesFromFolder(entry.path, includeSubdirectories))
        
    return files


def getOutputFileLocationForYamlFilename(path):
    filename = os.path.basename(path)
    return os.path.join(OUTPUT, filename.split(".")[0]+"Shape.ttl")

def getValidationYamlLocation():
    return YML_DIR_PATH+"/validation/"

def iterateFolder(path):
    files = getYamlFilesFromFolder(path, True)
    for file in files:
        output = getOutputFileLocationForYamlFilename(file)
        print(file)
        data = parseYAML(file)
        writeShaclFile(prefixes, data, output)


if __name__ == "__main__":

    YML_DIR_PATH = os.path.abspath(sys.argv[1])
    VERSION_INDICATION = os.path.basename(sys.argv[1])
    OUTPUT = os.path.abspath("../yaml2shacl/"+ VERSION_INDICATION +"/")

    URL_PREFIX = os.environ.get("WEBSITE_PREFIX", "http://w3id.org/gaia-x/")+VERSION_INDICATION+"/vocab"
    
    if not os.path.exists(OUTPUT):
        os.makedirs(OUTPUT)

    dataTypeAbbreviation = os.path.join(YML_DIR_PATH, "validation/dataTypeAbbreviation.yaml")
    simple_data_types = list(parseYAML(dataTypeAbbreviation).keys())

    file_loader = FileSystemLoader(os.path.join(YML_DIR_PATH,'validation/'))
    env = Environment(loader=file_loader)
    template = env.get_template('prefixes.yaml.jinja2')
    rendered_prefixes = template.render(url_prefix=URL_PREFIX)
    prefixes = yaml.safe_load(rendered_prefixes)

    iterateFolder(YML_DIR_PATH)






