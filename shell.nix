{ sources ? import nix/sources.nix }:
let nixpkgs =  import sources.nixpkgs {};
in nixpkgs.mkShell {
    packages = [ nixpkgs.gitlab-runner ];
}