import rdflib

g = rdflib.Graph()
# <RDF4J_URL>/repositories/<ID>/rdf-graphs/<NAME>
# g.parse("http://localhost:7200/repositories/reprise_demo/rdf-graphs/default")
g.parse("http://localhost:7200/repositories/reprise_demo/rdf-graphs/service?default")

knows_query = """

PREFIX ns1: <https://schemas.abc-federation.gaia-x.community/wip/json-validation/provider.json#>
PREFIX service: <https://schemas.abc-federation.gaia-x.community/wip/json-validation/service-offering.json#>

select ?services
where {
	<did:web:compliance4tf.byo-networks.net/tf/participant/a2e3df0baa66d4c02c484effa180095c7d62ec5b2d84c21f1c5ecb504eb1e5ce.jsonld> ns1:hasServices  ?services .
    ?services service:keyword 'service-type:object-storage'
}

"""

qres = g.query(knows_query)

for row in qres:
    print(f"Services : {row.services}")