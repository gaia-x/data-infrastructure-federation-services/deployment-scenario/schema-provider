# Launch the app

## With pipenv

```bash
$ pipenv install
$ pipenv shell
$ export FLASK_APP=main.py
$ export FLASK_ENV=development
$ flask run
```

# Dev environment

## IntelliJ

https://www.jetbrains.com/help/idea/pipenv.html

# Scénario de test

1. Charger dans un repository GraphDB le graphe suivant : jsonld-examples/catalog.json au format **JSON-LD**
2. Pour récupérer les services du provider qui sont de type object-storage, executer la requête suivante :

```
PREFIX ns1: <https://schemas.abc-federation.gaia-x.community/wip/json-validation/provider.json#>
PREFIX service: <https://schemas.abc-federation.gaia-x.community/wip/json-validation/service-offering.json#>

# Je veux chercher les services de type object-storage
select ?services
where {
    # le . est l'équivalent du AND
    <did:web:compliance4tf.byo-networks.net/tf/participant/a2e3df0baa66d4c02c484effa180095c7d62ec5b2d84c21f1c5ecb504eb1e5ce.jsonld> ns1:hasServices  ?services .
    ?services service:keyword 'service-type:object-storage'
}
```

3. Exécutez le script python db_query.py pour obtenir le même résultat (bootstrap python permettant de requêter une base graph)
