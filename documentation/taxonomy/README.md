```mermaid
flowchart TD
    entity --> resource & service_offering
    resource --> physical_resource & virtual_resource

    physical_resource --> datacenter & switch & server & rack & car & train
 
    virtual_resource --> software & license & configuration & dataset
 
    service_offering --> platform & infrastructure
 
    infrastructure --> compute & network & storage
 
    compute --> baremetal & virtual_machine & container
 
    storage --> block & file & object
 
    network --> load_balancer & interconnection
 
    platform --> identity_access & database & orchestrator
 
    orchestrator --> K8s & mesos
```
