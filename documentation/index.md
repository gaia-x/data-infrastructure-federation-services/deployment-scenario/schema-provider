# Self-Description Landing Page

Welcome to the Gaia-X Self-Description landing page.

This web site contains **Technical implementation** of taxonomy and attributes as **Ontologies** and **Validation Shapes**, including **Documentation**.

It is organized in around several versions of implementations: **wip** for work in progress and **22.04**.

For each version, you will find a page for:

- Reference of all SD Attributes
- SHACL Shapes usefull to validate data objects
- JSON schemas usefull to validate data objects
- Ontologies (OWL)
- Additional documentation specific to a version

## Content to be added

- **Introduction** to Self-Descriptions and basic **Terminology**
- **Taxonomy** of Service Offering and Resource classes
- **Attributes** for describing any of these classes
- **Tools** for End Users
- A general **context** web asset referenced in JSON-LD data files:

Proposal of context published at *https://schemas.abc-federation.gaia-x.community/wip/vocab/context.jsonld*

```json
{
  "@context": {
    "dct": "http://purl.org/dc/terms/",
    "sh": "http://www.w3.org/ns/shacl#",
    "xsd": "http://www.w3.org/2001/XMLSchema#",
    "vcard": "http://www.w3.org/2006/vcard/ns#",
    "dcat": "http://www.w3.org/ns/dcat#",
    "did": "https://www.w3.org/TR/did-core/#",
    "gax-core": "https://schemas.abc-federation.gaia-x.community/wip/vocab/core#",
    "gax-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#",
    "gax-service": "https://schemas.abc-federation.gaia-x.community/wip/vocab/service#",
    "gax-resource": "<https://schemas.abc-federation.gaia-x.community/wip/vocab/resource#>",
    "gax-validation": "https://schemas.abc-federation.gaia-x.community/wip/vocab/validation#",
    "gax-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#"
  }
}
```

Then used as :

```json
{
 "@context": "https://schemas.abc-federation.gaia-x.community/wip/vocab/context.jsonld"
 ...
}
```

## Tutorials

### Generate a self-description data

In this, tutorial, you will create a `LegalPerson` data file in JSON-LD.

1. Download the SHACL shape file for the `LegalPerson` type. Go to the [SHACL shapes page](https://schemas.abc-federation.gaia-x.community/wip/shacl-shapes.html) then download the [legal-personShape.ttl](https://schemas.abc-federation.gaia-x.community/wip/validation/legal-personShape.ttl) file.
2. To generate a JSON-LD file matching the `LegalPerson` schema, we will use an online [SHACL parser](https://gaia-x.fit.fraunhofer.de/)

- Then upload the `legal-personShape.ttl`
- Fill-in the web form

3. Download the generated JSON-LD file

You should obtain something similar to this file:

```json
{
  "@context": {
    "cred": "https://www.w3.org/2018/credentials/#",
    "gax-service": "https://schemas.abc-federation.gaia-x.community/wip/vocab/service#",
    "gax-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
    "xsd": "http://www.w3.org/2001/XMLSchema#",
    "gax-validation": "https://schemas.abc-federation.gaia-x.community/wip/vocab/validation#",
    "vcard": "http://www.w3.org/2006/vcard/ns#",
    "gax-core": "https://schemas.abc-federation.gaia-x.community/wip/vocab/core#",
    "gax-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#",
    "dct": "http://purl.org/dc/terms/",
    "sh": "http://www.w3.org/ns/shacl#",
    "gax-node": "https://schemas.abc-federation.gaia-x.community/wip/vocab/node#",
    "dcat": "http://www.w3.org/ns/dcat#",
    "gax-resource": "https://schemas.abc-federation.gaia-x.community/wip/vocab/resource#",
    "did": "https://www.w3.org/TR/did-core/#"
  },
  "@id": "http://example.org/LegalPerson-uu2ww3av8op7trdillwzo3",
  "@type": "gax-participant:LegalPerson",
  "gax-participant:registrationNumber": {
    "@value": "1234",
    "@type": "xsd:string"
  },
  "gax-participant:legalAddress": {
    "@type": "vcard:Address",
    "vcard:country-name": {
      "@value": "France",
      "@type": "xsd:string"
    }
  },
  "gax-participant:headquarterAddress": {
    "@type": "vcard:Address",
    "vcard:country-name": {
      "@value": "France",
      "@type": "xsd:string"
    }
  },
  "gax-participant:termsAndConditions": {
    "@type": "gax-core:TermsAndConditions",
    "gax-core:Value": {
      "@value": "https://example.com/terms-and-conditions",
      "@type": "xsd:anyURI"
    },
    "gax-core:hash": {
      "@value": "232RRET4R32",
      "@type": "xsd:string"
    }
  }
}
```

### Obtain a Verifiable Credentials for the SD data (To be validated)

As documented on the [Gaia-X Compliance lab](https://gitlab.com/gaia-x/lab/compliance/gx-compliance#how-to-create-self-descriptions), use the JSON-LD file above and put it inside a VerifiableCredential structure :

```json
{
  "@context": ["http://www.w3.org/ns/shacl#", "http://www.w3.org/2001/XMLSchema#", "http://w3id.org/gaia-x/participant#"],
  "@id": "http://example.org/participant-dp6gtq7i75lmk9p4j2tfg",
  "@type": ["VerifiableCredential", "LegalPerson"],
  "credentialSubject": {
          PUT THE JSON LD here
          ...
  }
}
```

Then follow the [Gaia-X Compliance lab](https://gitlab.com/gaia-x/lab/compliance/gx-compliance#how-to-create-self-descriptions) steps:

- normalize the file
- sign it with a private key related to a `did:web` object
- call the compliance service to get a signed `complianceCredential`

### Validate a self-description

Using [PySHACL CLI](https://github.com/RDFLib/pySHACL), you can validate a JSON-LD SD data content conforms to a SHACL shape file.

The examples below use the [legal-personShape.ttl](https://schemas.abc-federation.gaia-x.community/wip/validation/legal-personShape.ttl) file.

#### Example of valid data structure

`valid-data.jsonld` file:

```json
{
  "@context": {
    "dct": "http://purl.org/dc/terms/",
    "sh": "http://www.w3.org/ns/shacl#",
    "xsd": "http://www.w3.org/2001/XMLSchema#",
    "gax-core": "https://schemas.abc-federation.gaia-x.community/wip/vocab/core#",
    "gax-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#",
    "gax-service": "https://schemas.abc-federation.gaia-x.community/wip/vocab/service#",
    "gax-resource": "<https://schemas.abc-federation.gaia-x.community/wip/vocab/resource#>",
    "gax-validation": "https://schemas.abc-federation.gaia-x.community/wip/vocab/validation#",
    "gax-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
    "vcard": "http://www.w3.org/2006/vcard/ns#",
    "dcat": "http://www.w3.org/ns/dcat#",
    "did": "https://www.w3.org/TR/did-core/#"
  },
  "@id": "http://example.org/LegalPerson-vrfx4i6bw9m51vhizaqqhu",
  "@type": "gax-participant:LegalPerson",
  "gax-participant:registrationNumber": {
    "@value": "123456789"
  },
  "gax-participant:legalAddress": {
    "@type": "vcard:Address",
    "vcard:country-name": {
      "@value": "Germany",
      "@type": "xsd:string"
    },
    "vcard:street-address": {
      "@value": "ACEM de",
      "@type": "xsd:string"
    },
    "vcard:postal-code": {
      "@value": "12163",
      "@type": "xsd:string"
    },
    "vcard:locality": {
      "@value": "Berlin",
      "@type": "xsd:string"
    }
  },
  "gax-participant:termsAndConditions": {
    "@type": "gax-core:TermsAndConditions",
    "gax-core:Value": {
      "@value": "http://Berlin",
      "@type": "xsd:anyURI"
    },
    "gax-core:hash": {
      "@value": "Berlin",
      "@type": "xsd:string"
    }
  },
  "gax-participant:headquarterAddress": {
    "@type": "vcard:Address",
    "vcard:country-name": {
      "@value": "Germany",
      "@type": "xsd:string"
    },
    "vcard:street-address": {
      "@value": "DEF",
      "@type": "xsd:string"
    },
    "vcard:postal-code": {
      "@value": "12163",
      "@type": "xsd:string"
    },
    "vcard:locality": {
      "@value": "Paris",
      "@type": "xsd:string"
    }
  }
}
```

```sh
pyshacl -im -a --abort -it -m -s legal-personShape.ttl valid-data.jsonld
# Conforms: True
```

#### Example of INVALID data structure

We will change the type of `registrationNumber` attribute, and remove the `termsAndConditions` attributes
`invalid-data.jsonld` file:

```json
{
  "@context": {
    "dct": "http://purl.org/dc/terms/",
    "sh": "http://www.w3.org/ns/shacl#",
    "xsd": "http://www.w3.org/2001/XMLSchema#",
    "gax-core": "https://schemas.abc-federation.gaia-x.community/wip/vocab/core#",
    "gax-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#",
    "gax-service": "https://schemas.abc-federation.gaia-x.community/wip/vocab/service#",
    "gax-resource": "<https://schemas.abc-federation.gaia-x.community/wip/vocab/resource#>",
    "gax-validation": "https://schemas.abc-federation.gaia-x.community/wip/vocab/validation#",
    "gax-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
    "vcard": "http://www.w3.org/2006/vcard/ns#",
    "dcat": "http://www.w3.org/ns/dcat#",
    "did": "https://www.w3.org/TR/did-core/#"
  },
  "@id": "http://example.org/LegalPerson-vrfx4i6bw9m51vhizaqqhu",
  "@type": "gax-participant:LegalPerson",
  "gax-participant:registrationNumber": {
    "@value": 123456789
  },
  "gax-participant:legalAddress": {
    "@type": "vcard:Address",
    "vcard:country-name": {
      "@value": "Germany",
      "@type": "xsd:string"
    },
    "vcard:street-address": {
      "@value": "ACEM de",
      "@type": "xsd:string"
    },
    "vcard:postal-code": {
      "@value": "12163",
      "@type": "xsd:string"
    },
    "vcard:locality": {
      "@value": "Berlin",
      "@type": "xsd:string"
    }
  },
  "gax-participant:headquarterAddress": {
    "@type": "vcard:Address",
    "vcard:country-name": {
      "@value": "Germany",
      "@type": "xsd:string"
    },
    "vcard:street-address": {
      "@value": "DEF",
      "@type": "xsd:string"
    },
    "vcard:postal-code": {
      "@value": "12163",
      "@type": "xsd:string"
    },
    "vcard:locality": {
      "@value": "Paris",
      "@type": "xsd:string"
    }
  }
}
```

```sh
pyshacl -im -a --abort -it -m -s legal-personShape.ttl invalid-data.jsonld
# Conforms: False
```
