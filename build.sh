#!/bin/bash

export -p WEBSITE_PREFIX="https://compliance4tf.byo-networks.net/tf/"
python3 toolchain/check_yaml.py single-point-of-truth/yaml/wip/ single-point-of-truth/yaml/wip/validation
cd toolchain/ontology_generation/ 
python3 yaml2ttl.py ../../single-point-of-truth/yaml/wip
cd ../
python3 yaml2json.py ../single-point-of-truth/yaml/wip
cd ../yaml2ontology/wip/
./make_all_variant


